package com.gestion.de.ahorro.GestionUsuarios.controllers;

import java.util.List;

import com.gestion.de.ahorro.GestionUsuarios.model.Users;
import com.gestion.de.ahorro.GestionUsuarios.repositories.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/GestionUsuarios")
public class UserController {
    @Autowired
    UsersRepository usersRepository;

    @GetMapping("/users")
    public ResponseEntity<List<Users>> findIncomesByUserId(@RequestParam(required = true) long id) {
        List<Users> users = usersRepository.findbyUserID(id);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<Users> registerIncome(@RequestBody Users users) {
        try {
            Users created = usersRepository.save(
                    new Users(users.getType(), users.getUserID(), users.getName(), users.getAccountID(), users.getMoney()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
