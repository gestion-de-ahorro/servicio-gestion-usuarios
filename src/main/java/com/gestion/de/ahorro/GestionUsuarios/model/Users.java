package com.gestion.de.ahorro.GestionUsuarios.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users")
public class Users {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "type")
    private String type;

    @Column(name = "userID")
    private String userID;

    @Column(name = "name")
    private String name;

    @Column(name = "accountID")
    private long accountID;

    @Column(name = "money")
    private Double money;

    public Users(String type, String userID, String name, long accountID, Double money){
        this.type = type;
        this.userID = userID;
        this.name = name;
        this.accountID = accountID;
        this.money = money;
    }
}
