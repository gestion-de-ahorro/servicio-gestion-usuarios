package com.gestion.de.ahorro.GestionUsuarios;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/GestionUsuarios")

public class MainController {
    @GetMapping
    public String saludo(){
        return "Hello from Gestión de Usuarios";
    }
}
