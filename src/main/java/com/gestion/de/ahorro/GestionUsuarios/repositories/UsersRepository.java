package com.gestion.de.ahorro.GestionUsuarios.repositories;

import java.util.List;

import com.gestion.de.ahorro.GestionUsuarios.model.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsersRepository extends JpaRepository<Users, Long>{
    @Query(
        value = "SELECT * FROM users i WHERE i.userID = ?1",
        nativeQuery = true
    )
    List<Users> findbyUserID(long userID);
}
